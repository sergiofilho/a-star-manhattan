﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Media;

namespace TestePathFinder.Square
{
    public class SquareBackgroundConverter: IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Square square = (Square)value;
            if (square.IsWall)            
                return new SolidColorBrush(Colors.Red);
            else if(square.IsOrigin)
                return new SolidColorBrush(Colors.Green);
            else if (square.IsDestiny)
                return new SolidColorBrush(Colors.Blue);
            return new SolidColorBrush(Colors.White);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
