﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TestePathFinder.Square
{    
    public partial class Square : UserControl
    {
        public int Line { get; set; }
        public int Column { get; set; }

        public Square Parent { get; set; }
        public int F { get; set; }
        public int G { get; set; }
        public int H { get; set; }        

        public bool IsWall { get; set; }
        public bool IsOrigin { get; set; }
        public bool IsDestiny { get; set; }
                

        public Square()
        {
            InitializeComponent();        
        }

        public Square(int line, int column)
        {
            InitializeComponent();
            this.Line = line;
            this.Column = column;
        }

        public Border GetMainBorder()
        {
            return mainBorder;
        }
               
        private void Border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {            
            if (IsWall)
            {
                IsWall = false;
                IsOrigin = true;
                mainBorder.Background = new SolidColorBrush(Colors.GreenYellow);                
            }
            else if (IsOrigin)
            {
                IsOrigin = false;
                IsDestiny = true;
                mainBorder.Background = new SolidColorBrush(Colors.Blue);
            }
            else if (IsDestiny)
            {
                IsDestiny = false;
                mainBorder.Background = new SolidColorBrush(Colors.White);
            }
            else
            {
                IsWall = true;
                mainBorder.Background = new SolidColorBrush(Colors.Red);
            }
        }

        

    }
}
