﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using Squaree = TestePathFinder.Square.Square;

namespace TestePathFinder
{
    
    public partial class MainWindow : Window
    {
        private Squaree[,] Squares { get; set; }        
        private int verticalMovementCost = 10;
        private int diagonalMovementCost = 14;

        public MainWindow()
        {
            InitializeComponent();
            CreateSquares();
        }
                
        private void CreateSquares()
        {
            gdBoard.Children.Clear();
            int lines = Int32.Parse(tbxLinesAmount.Text);
            int columns = Int32.Parse(tbxColumnsAmount.Text);

            Squares = new TestePathFinder.Square.Square[lines, columns];

            gdBoard.RowDefinitions.Clear();
            gdBoard.ColumnDefinitions.Clear();

            for (int line = 0; line < lines; line++)
            {
                RowDefinition rowDefinition = new RowDefinition();
                rowDefinition.Height = new GridLength(1.0, GridUnitType.Star);
                gdBoard.RowDefinitions.Add(rowDefinition);

                ColumnDefinition colDefinition = new ColumnDefinition();
                colDefinition.Width = new GridLength(1.0, GridUnitType.Star);
                gdBoard.ColumnDefinitions.Add(colDefinition);

                for (int column = 0; column < columns; column++)
                {
                    Squaree square = new TestePathFinder.Square.Square(line, column);                    
                    Squares[line, column] = square;

                    Grid.SetRow(square, line);
                    Grid.SetColumn(square, column);                    
                    gdBoard.Children.Add(square);
                }
            }

        }

        private void btnZoomLess_Click(object sender, RoutedEventArgs e)
        {
            boardWrapper.Width = boardWrapper.Height = boardWrapper.Width * 0.9;
        }

        private void btnZoomMore_Click(object sender, RoutedEventArgs e)
        {
            boardWrapper.Width = boardWrapper.Height = boardWrapper.Width * 1.1;
        }

        private void btnCreateBoard_Click(object sender, RoutedEventArgs e)
        {
            CreateSquares();
        }

        private TestePathFinder.Square.Square GetOriginSquare()
        {
            int lines = Int32.Parse(tbxLinesAmount.Text);
            int columns = Int32.Parse(tbxColumnsAmount.Text);
            for (int line = 0; line < lines; line++)
            {
                for (int column = 0; column < columns; column++)
                {
                    if (Squares[line, column].IsOrigin)                    
                        return Squares[line, column];                    
                }
            }
            return null;
        }

        private TestePathFinder.Square.Square GetDestinySquare()
        {
            int lines = Int32.Parse(tbxLinesAmount.Text);
            int columns = Int32.Parse(tbxColumnsAmount.Text);
            for (int line = 0; line < lines; line++)
            {
                for (int column = 0; column < columns; column++)
                {
                    if (Squares[line, column].IsDestiny)
                        return Squares[line, column];
                }
            }
            return null;
        }

        private void btnSearchPath_Click(object sender, RoutedEventArgs e)
        {
            List<Squaree> openList = new List<Squaree>();
            List<Squaree> closedList = new List<Squaree>();

            var originSquare = GetOriginSquare();
            var destinySquare = GetDestinySquare();
                        
            Squaree nearestOpenListSquare = originSquare;
            while (nearestOpenListSquare != null && nearestOpenListSquare != destinySquare)
            {
                closedList.Add(nearestOpenListSquare);//na primeira vez isso coloca origem na lista fechada
                if (openList.Contains(nearestOpenListSquare)) 
                {
                    openList.Remove(nearestOpenListSquare);
                    if(nearestOpenListSquare != destinySquare)
                        nearestOpenListSquare.GetMainBorder().Background = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.Orange);
                }
                nearestOpenListSquare = CheckNodesAround(nearestOpenListSquare, destinySquare, openList, closedList);
            }

            ShowResult(nearestOpenListSquare, originSquare, destinySquare);
        }

        private void ShowResult(Squaree lastCheckedSquare, Squaree originSquare, Squaree destinySquare)
        {
            Squaree squareToOrigin = lastCheckedSquare.Parent;
            while (squareToOrigin != originSquare)
            {
                squareToOrigin.GetMainBorder().Background = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.Yellow);
                squareToOrigin = squareToOrigin.Parent;
            }
            
            if (lastCheckedSquare == destinySquare)
                MessageBox.Show("encontrado");
            else if (lastCheckedSquare == null)
                MessageBox.Show("caminho bloqueado");
            else
                MessageBox.Show("resultado não esperado");
        }

        //retorna o square com menor F da lista aberta ou o square destino caso seja o fim ou null caso caminho bloqueado
        private Squaree CheckNodesAround(Squaree square, Squaree destinySquare, List<Squaree> openList, List<Squaree> closedList)
        {   
            int lines = Int32.Parse(tbxLinesAmount.Text);
            int columns = Int32.Parse(tbxColumnsAmount.Text);

            //QUADRADO ACIMA
            if (square.Line > 0)
                CheckNeighborSquare(square, Squares[square.Line - 1, square.Column], destinySquare, openList, closedList, verticalMovementCost);            
            //QUADRADO ACIMA E A DIREITA
            if(square.Line > 0 && square.Column < columns - 1)
                CheckNeighborSquare(square, Squares[square.Line - 1, square.Column + 1], destinySquare, openList, closedList, diagonalMovementCost);
            //QUADRADO A DIREITA
            if(square.Column < columns - 1)
                CheckNeighborSquare(square, Squares[square.Line, square.Column + 1], destinySquare, openList, closedList, verticalMovementCost);            
            //QUADRADO A DIREITA E ABAIXO
            if (square.Column < columns - 1 && square.Line < lines - 1)
                CheckNeighborSquare(square, Squares[square.Line + 1, square.Column + 1], destinySquare, openList, closedList, diagonalMovementCost);
            //QUADRADO ABAIXO
            if (square.Line < lines - 1)
                CheckNeighborSquare(square, Squares[square.Line + 1, square.Column], destinySquare, openList, closedList, verticalMovementCost);
            //QUADRADO A ESQUERDA E ABAIXO 
            if (square.Column > 0 && square.Line < lines - 1)
                CheckNeighborSquare(square, Squares[square.Line + 1, square.Column - 1], destinySquare, openList, closedList, diagonalMovementCost);
            //QUADRADO A ESQUERDA
            if (square.Column > 0)
                CheckNeighborSquare(square, Squares[square.Line, square.Column - 1], destinySquare, openList, closedList, verticalMovementCost);
            //QUADRADO ACIMA E A ESQUERDA
            if (square.Column > 0 && square.Line > 0)
                CheckNeighborSquare(square, Squares[square.Line - 1, square.Column - 1], destinySquare, openList, closedList, diagonalMovementCost);
            
            return GetLowestF(openList);
        }

        private void CheckNeighborSquare(Squaree currentSquare, Squaree neighborSquare, Squaree destinySquare, List<Squaree> openList, List<Squaree> closedList, int moveCost)
        {
            //IGNORA OBSTÁCULOS E VIZINHOS QUE JÁ ESTÃO NA LISTA FECHADA
            if (!neighborSquare.IsWall && !closedList.Contains(neighborSquare))
            {
                var parentG = currentSquare.Parent != null ? currentSquare.Parent.G : 0;
                var g = moveCost + parentG;
                var h = GetDistanceManhatan(neighborSquare, destinySquare);
                var f = g + h;

                //ADICIONA NÓ VIZINHO NA LISTA ABERTA, CASO NÃO ESTEJA
                if (!openList.Contains(neighborSquare))
                {
                    neighborSquare.G = g;
                    neighborSquare.H = h;
                    neighborSquare.F = f;
                    neighborSquare.Parent = currentSquare;
                    openList.Add(neighborSquare);
                    
                    if(neighborSquare != destinySquare)
                        neighborSquare.GetMainBorder().Background = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.LightGray);
                }
                //CASO JÁ ESTEJA, VERIFICA SE O NÓ ATUAL É UM PAI MELHOR E SUBSTITUI
                else if (g < neighborSquare.G)
                {
                    neighborSquare.G = g;
                    neighborSquare.F = f;
                    neighborSquare.Parent = currentSquare;
                }
            }
        }

        private int GetDistanceManhatan(Squaree square1, Squaree square2)
        {
            var dHor = Math.Abs(square1.Column - square2.Column);
            var dVer = Math.Abs(square1.Line - square2.Line);
            return dHor + dVer;
        }

        private Squaree GetLowestF(List<Squaree> openList)
        {
            Squaree retVal = null;
            foreach (Squaree square in openList)            
                if (retVal == null || square.F < retVal.F)
                    retVal = square;            
            return retVal;
        }
        
    }
}
